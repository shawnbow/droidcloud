#encoding=utf-8
from django.db import models

class Publisher(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=50)
    city = models.CharField(max_length=60)
    state_province = models.CharField(max_length=30)
    country = models.CharField(max_length=50)
    website = models.URLField(blank = True)
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['name']

class Author(models.Model):
    first_name = models.CharField(verbose_name = u'名',max_length=30)
    last_name = models.CharField(verbose_name = u'姓', max_length=40)
    email = models.EmailField()
    def __unicode__(self):
        return u'%s %s' % (self.last_name, self.first_name)

class BookManager(models.Manager):
    def title_count(self, keyword):
        return self.filter(title__icontains=keyword).count()

class Book(models.Model):
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author)
    publisher = models.ForeignKey(Publisher)
    publication_date = models.DateField()
    #forumsite = models.URLField(blank = True)
    def __unicode__(self):
        return self.title
