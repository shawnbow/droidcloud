from django.http import HttpResponse
from django.shortcuts import render_to_response
from models import Book
from django.views.generic import ListView
from models import Publisher

class PublisherList(ListView):
    model = Publisher
    template_name = 'publisher_list_page.html'

def search(request):
    errors = []
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            errors.append('Enter a search term.')
        elif len(request.GET['q']) > 20:
            errors.append('Please enter a search term less than 20 chars.')
        else:
            books = Book.objects.filter(title__icontains=q)
            return render_to_response('search_results.html', {'books': books, 'query': q})

    return render_to_response('search_form.html', {'errors': errors})
