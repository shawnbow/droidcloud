#encoding=utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
import droidcloud.books.views
import droidcloud.contact.views

admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
     #url(r'^$', 'droidcloud.views.home', name='home'),
     #url(r'^droidcloud/', include('droidcloud.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^search/$', 'droidcloud.books.views.search'),
    url(r'^contact/$', 'droidcloud.contact.views.contact'),
    url(r'^publishers/$', droidcloud.books.views.PublisherList.as_view())
)
